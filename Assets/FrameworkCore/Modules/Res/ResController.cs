﻿/*
 *  date: 2018-07-12
 *  author: John-chen
 *  cn: 资源管理器
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 资源管理器
    /// 管理资源加载和卸载
    /// </summary>
    public class ResController : Singleton<ResController>
    {
        /*
         *  所有加载的资源：Dic<完整路径, 加载AssetBundle>
         *  设置资源的接口：AddRes(string resName, AssetBundle abRes);
         *  添加引用
         *  删除引用,当引用为0时,删除所有资源
         *  全部用事件来处理
         *
         *
         */
        //public void 


        
        /// <summary>
        /// 初始化
        /// </summary>
        protected override void Init()
        {

        }


    }
}