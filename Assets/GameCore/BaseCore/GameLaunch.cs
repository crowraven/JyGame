﻿/*
 *  date: 2018-07-20
 *  author: John-chen
 *  cn: 游戏启动类
 *  en: todo:
 */

using UnityEngine;
using JyFramework;

namespace GameCore.BaseCore
{
    /// <summary>
    /// 游戏启动类
    /// </summary>
    public class GameLaunch : MonoBehaviour
    {
        /// <summary>
        /// 核心类
        /// </summary>
        public JyApp App { get { return _app;} }

        private void Awake()
        {
            _app = JyApp.CreateInstance(gameObject, "JyApp");
            _app.InitGame();

            DontDestroyOnLoad(gameObject);
            GameApp.InitSelf(gameObject);
        }

        private void Start()
        {
            _app.StartGame();
        }

        private void Update()
        {
            //Debug.Log("GameApp Update !");
            _app.UpdateGame(Time.deltaTime);
        }

        private void OnDestroy()
        {
            _app.ExitGame();
        }

        private JyApp _app;
    }
}