﻿/*
 *  date: 2018-06-29
 *  author: John-chen
 *  cn: 供客户端使用的UI管理器
 *  en: todo:
 */

using JyFramework;

namespace GameCore.BaseCore
{
    /// <summary>
    /// UI管理器
    /// </summary>
    public class UIMgr : SingletonMgr<UIMgr>, IUIFunction
    {
        /// <summary>
        /// 获取窗口
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetWindow<T>() where T : BaseWindow
        {
            return _uiCtrl.GetWindow<T>();
        }

        /// <summary>
        /// 显示窗口
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <param name="args"></param>
        public T Show<T>(WndAction action = null) where T : BaseWindow, new()
        {
            return _uiCtrl.Show<T>(action);
        }

        /// <summary>
        /// 关闭窗口
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <param name="args"></param>
        public T Hide<T>(WndAction action = null) where T : BaseWindow
        {
            return _uiCtrl.Hide<T>(action);
        }

        /// <summary>
        /// 关闭所有窗口
        /// </summary>
        public void HideAll()
        {
            _uiCtrl.HideAll();
        }

        /// <summary>
        /// 清理所有窗口
        /// </summary>
        public void ClearAll()
        {
            _uiCtrl.ClearAll();
        }

        /// <summary>
        /// 初始化
        /// </summary>
        protected override void Init()
        {
            _uiCtrl = UIController.Ins;
            _uiCtrl.CreateUIRoot();
            UIConfig.ConfigPath();
        }

        private UIController _uiCtrl;
    }
}